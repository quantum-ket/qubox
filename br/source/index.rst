Simulador QuBOX
===============

O **QuBOX** é um simulador portátil de computação quântica, desenvolvido para a plataforma de programação **Ket**. Acelerado por GPU, ele suporta dois modelos de simulação: denso e esparso, permitindo simular sistemas com mais de 30 qubits.

Com o QuBOX, você pode acelerar seus experimentos de computação quântica utilizando os simuladores da Quantuloop, e tudo isso com poucas linhas de código, graças à integração com a plataforma Ket.

Para aprender mais sobre o Ket, consulte a `documentação <https://quantumket.org>`_. Se você está começando na computação quântica, não deixe de visitar nossa plataforma de ensino: `aprenda.quantumket.org <https://aprenda.quantumket.org>`_.

Essa iniciativa é fruto da parceria entre a startup **Quantuloop** e o **Grupo de Computação Quântica da UFSC**, que disponibilizam o acesso remoto e gratuito ao simulador QuBOX.

.. image:: img/qubox.png
    :width: 100%
    :class: no-scaled-link


.. grid:: 2

    .. grid-item-card:: Use o QuBOX
        :link: qubox
        :link-type: doc
        :text-align: center
    

    .. grid-item-card:: Aprenda Ket
        :link: https://aprenda.quantumket.org
        :text-align: center

.. grid:: 2

    .. grid-item-card:: Sobre o Projeto
        :link: projeto
        :link-type: doc
        :text-align: center
    

    .. grid-item-card:: Sobre o GCQ-UFSC
        :link: gcq
        :link-type: doc
        :text-align: center

 
    
Apoio
-----

Esse projeto é uma parceria entre o **Centro de Ciências Físicas e Matemáticas** (CFM) e o **Departamento de Informática e Estatística** (INE/CTC) da **Universidade Federal de Santa Catarina** (UFSC) através do **Grupo de Computação Quântica - UFSC**.

.. list-table:: 
    :class: only-light
    :align: center
    :widths: 8 8 8 8

    * - .. figure:: _static/brasao_UFSC_vertical_extenso.svg

      - .. figure:: _static/cfm.jpg

      - .. figure:: _static/ine.png

      - .. figure:: _static/quantuloop.svg


.. list-table:: 
    :class: only-dark
    :align: center
    :widths: 8 8 8 9

    * - .. figure:: _static/brasao_UFSC_vertical_sigla_fundoescuro.svg

      - .. figure:: _static/cfm.jpg

      - .. figure:: _static/ine_fundoescuro.png

      - .. figure:: _static/quantuloop_white.svg


.. topic:: E-mail para contato

    | QuBOX UFSC: qubox.cfm@contato.ufsc.br
    | Quantuloop: contact@quantuloop.com

.. toctree::
    :maxdepth: 2
    :hidden:   

    qubox
    ket
    projeto
    gcq  
    
.. TODO: alterar link do GCQ após atualização do site.

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Links

    Documentação do Ket (inglês) <https://quantumket.org>
    Aprenda Ket <https://aprenda.quantumket.org>
    GCQ-UFSC <http://gcq.ufsc.br>
    CFM-UFSC <https://cfm.ufsc.br>
    INE/CTC-UFSC <https://ine.ufsc.br>

