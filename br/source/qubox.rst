Simulador QuBOX
===============

QuBOX é um hardware portátil que embarca simuladores desenvolvido pela Quantuloop. Projetado para auxiliar no ensino e pesquisa de computação quântica, o simulador QuBOX é uma solução prática para acelerar a execução quântica de aplicações escritas em Ket.

O Simulador QuBOX foi pensado para ser levado para sala de aula ou laboratório, sem a necessidade de nenhuma infraestrutura além do acesso a rede e energia. Na UFSC, o simulador QuBOX está disponível para acesso em nuvem, sendo necessário poucas configurações para que você consiga acelerar sua aplicação quântica.

Especificações
--------------

O simulador oferece dois modos de simulação, **esparso** e **denso**, cujas complexidades de tempo de simulação estão relacionadas a diferentes características da computação quântica:

1. **Modo Esparso**: O tempo de simulação depende do número de estados da base computacional necessários para representar o estado quântico.
2. **Modo Denso**: O tempo de simulação depende diretamente do número de qubits no sistema.

Em termos matemáticos, a complexidade de simular a preparação de um estado :math:`\sum_{k=0}^{2^n-1}\alpha_k\left|k\right>`, onde :math:`n` é o número de qubits, aumenta de forma:

- **Exponencial** com :math:`n` no modo **denso**.
- **Log-linear** com o número de estados onde :math:`\alpha_k \neq 0` no modo **esparso**.

Por exemplo:

- Para preparar um estado GHZ :math:`\frac{1}{\sqrt{2}}(\left|0\dots0\right>+\left|1\dots1\right>)` de :math:`n` qubits, são necessárias :math:`n-1` portas CNOT. No modo **esparso**, cada porta CNOT tem complexidade de tempo constante em relação ao número de qubits, enquanto no modo **denso**, a complexidade é exponencial.
- Para preparar o estado :math:`\frac{1}{\sqrt{2^n}}\sum_{k=0}^{2^n-1}\left|k\right>` de :math:`n` qubits, são necessárias :math:`n` portas Hadamard, resultando em:
    - Complexidade :math:`O(2^n \log{2^n})` no modo **esparso**.
    - Complexidade :math:`O(n 2^n)` no modo **denso**.

.. note::

  O modo de simulação deve ser escolhido conforme as características específicas de cada algoritmo quântico.


.. note::

  A função :func:`~ket.operations.dump` está desabilitada no QuBOX, pois o resultado de uma operação de dump pode facilmente exceder 10GB de informação, tornando sua execução inviável em um ambiente remoto.

.. _Instalação e Configuração:

Instalação e Configuração
-------------------------

Para acessar o simulador, é necessário ter o Ket v0.8.1 ou mais recente e a biblioteca `Requests <https://pypi.org/project/requests/>`_. Ambos podem ser instalados usando o `pip` com os comandos abaixo:  

.. code-block:: bash

    pip install pip -U
    pip install ket-lang requests -U

A classe :class:`~ket.remote.Remote` é utilizada para acessar o simulador. Para isso, basta instanciar a classe e conectá-la a um :class:`~ket.base.Process`.  

No construtor da classe :class:`~ket.remote.Remote`, é necessário fornecer o URL :code:`https://qubox.ufsc.br/sim`. Além disso, ao conectar com o processo usando o método :meth:`~ket.remote.Remote.connect`, é preciso passar os argumentos nomeados :code:`num_qubits=` e :code:`simulator=`.  

Veja o exemplo abaixo:  

.. code-block:: python

    from ket import Process
    from ket.remote import Remote

    # Instancia a classe Remote com o URL do simulador
    qubox = Remote("https://qubox.ufsc.br/sim")

    # Conecta ao simulador especificando o número de qubits e o modelo
    process = Process(qubox.connect(num_qubits=2, simulator="quantuloop::dense"))

Os simuladores quânticos disponíveis são:  

- :code:`quantuloop::dense`: Simulador Denso
- :code:`quantuloop::sparse`: Simulador Esparso

Teste Você Mesmo
----------------

Se você está utilizando o Ket, usar o QuBOX é extremamente simples: basta alterar duas linhas de código.
O exemplo abaixo implementa um algoritmo quântico de estimação de fase, cujo resultado é uma aproximação do número π.
É possível aumentar o número de qubits (:code:`num_qubits`) para melhorar a precisão do resultado, mas isso também aumenta o tempo de computação.

Após seguir o procedimento de instalação do Ket, você pode executar o código abaixo e observar a diferença no tempo de execução entre o seu computador e o QuBOX.


.. code-block:: python

    from math import pi
    from functools import partial
    from ket import Process, Quant, H, X, control, PHASE, SWAP, adj, measure
    from ket.remote import Remote
    from time import time


    def qft(qubits: Quant, invert: bool = True):
        if len(qubits) == 1:
            H(qubits)
        else:
            *init, last = qubits
            H(last)
            for i, ctrl_qubit in enumerate(reversed(init)):
                with control(ctrl_qubit):
                    PHASE(pi / 2 ** (i + 1), last)
            qft(init, invert=False)
        if invert:
            size = len(qubits)
            for i in range(size // 2):
                SWAP(qubits[i], qubits[size - i - 1])


    def phase_estimator(oracle_gate, qubits: Quant) -> float:
        ctr = H(qubits[:-1])
        tgr = X(qubits[-1])
        precision = len(ctr)

        for i, c in enumerate(ctr):
            with control(c):
                oracle_gate(i, tgr)
        adj(qft)(ctr)

        return measure(reversed(ctr)).get() / 2**precision


    def oracle(phase: float, i: int, tgr):
        PHASE(2 * pi * phase * 2**i, tgr)


    estimate_pi = partial(phase_estimator, partial(oracle, pi / 10))


    if __name__ == "__main__":
        num_qubits = 25

        begin = time()
        local_execution = Process(simulator="dense", num_qubits=num_qubits, execution="batch")
        local_qubits = local_execution.alloc(num_qubits)
        local_result = estimate_pi(local_qubits) * 10
        end = time()
        print(f"Local simulation: result={local_result}, time {end-begin}s")

        begin = time()
        qubox = Remote("https://qubox.ufsc.br/sim")
        remote_process = Process(qubox.connect(num_qubits=num_qubits, simulator="quantuloop::dense"))
        remote_qubits = remote_process.alloc(num_qubits)
        remote_result = estimate_pi(remote_qubits) * 10
        end = time()

        print(f"QuBOX simulation: result={remote_result}, time {end-begin}s")
