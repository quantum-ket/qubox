QuBOX Simulator
===============

The **QuBOX** is a portable quantum computing simulator developed for the **Ket** programming platform. GPU-accelerated, it supports two simulation models: dense and sparse, enabling the simulation of systems with more than 30 qubits.

With QuBOX, you can accelerate your quantum computing experiments using Quantuloop simulators, all with just a few lines of code, thanks to the seamless integration with the Ket platform.

To learn more about Ket, visit the `documentation <https://quantumket.org>`_. If you are new to quantum computing, be sure to check out our educational platform: `learn.quantumket.org[in Portuguese] <https://aprenda.quantumket.org>`_.

This initiative is the result of a partnership between the startup **Quantuloop** and the **Quantum Computing Group at UFSC**, offering free and remote access to the QuBOX simulator.

.. image:: img/qubox.png
    :width: 100%
    :class: no-scaled-link

.. grid:: 2

    .. grid-item-card:: Getting Started with Ket QuBOX
        :link: qubox
        :link-type: doc
        :text-align: center
    

    .. grid-item-card:: Ket Platform
        :link: https://quantumket.org
        :text-align: center

.. grid:: 2

    .. grid-item-card:: About the Project
        :link: projeto
        :link-type: doc
        :text-align: center
    

    .. grid-item-card:: About GCQ-UFSC
        :link: gcq
        :link-type: doc
        :text-align: center

Support
-------

This project is a partnership between the **Center for Physical and Mathematical Sciences** (CFM) and the **Department of Informatics and Statistics** (INE/CTC) of the **Federal University of Santa Catarina** (UFSC) through the **Quantum Computing Group - UFSC**.

.. list-table:: 
    :class: only-light
    :align: center
    :widths: 8 8 8 9

    * - .. figure:: _static/brasao_UFSC_vertical_extenso.svg

      - .. figure:: _static/cfm.jpg

      - .. figure:: _static/ine.png

      - .. figure:: _static/quantuloop.svg


.. list-table:: 
    :class: only-dark
    :align: center
    :widths: 8 8 8 9

    * - .. figure:: _static/brasao_UFSC_vertical_sigla_fundoescuro.svg

      - .. figure:: _static/cfm.jpg

      - .. figure:: _static/ine_fundoescuro.png

      - .. figure:: _static/quantuloop_white.svg


.. topic:: Contact Emails

    | QuBOX UFSC: qubox.cfm@contato.ufsc.br
    | Quantuloop: contact@quantuloop.com

.. toctree::
    :maxdepth: 2
    :hidden:   

    qubox
    projeto
    gcq  

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Links

    Ket Documentation <https://quantumket.org>
    GCQ-UFSC <http://gcq.ufsc.br>
    CFM-UFSC <https://cfm.ufsc.br>
    INE/CTC-UFSC <https://ine.ufsc.br>

