FROM python:3.10 AS build

COPY ./ /workdir/
WORKDIR /workdir/

RUN pip install -U -r requirements.txt
RUN mkdir -p public
RUN sphinx-build -b html br/source public
RUN sphinx-build -b html en/source public/en
RUN cp index.php public/

FROM busybox

COPY  --from=build /workdir/public /public