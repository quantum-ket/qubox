#! /bin/bash
docker build --no-cache -t qubox_ufsc_website .
docker run -v $PWD:/mnt --rm qubox_ufsc_website /bin/sh -c "cp -r /public /mnt/"
